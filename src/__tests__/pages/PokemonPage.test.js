import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Pokemon from '../../pages/Pokemon';


configure({ adapter: new Adapter() });
describe('SearchForm', () => {
  it('should render the Pokemon component correctly in "debug" mode', () => {
    const component = shallow(<Pokemon debug />);
    expect(component).toMatchSnapshot();
  });
});



