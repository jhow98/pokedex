import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import PokemonItem from '../../components/PokemonItem';

configure({ adapter: new Adapter() });
describe('SearchForm', () => {
  it('should render the PokemonItem component correctly in "debug" mode', () => {
    const component = shallow(<PokemonItem debug />);
    expect(component).toMatchSnapshot();
  });
});
