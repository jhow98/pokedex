import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Loader from '../../components/Loader';


configure({ adapter: new Adapter() });
describe('SearchForm', () => {
  it('should render the Loader component correctly in "debug" mode', () => {
    const component = shallow(<Loader debug />);
    expect(component).toMatchSnapshot();
  });
});



