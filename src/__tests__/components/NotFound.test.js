import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import NotFound from '../../components/NotFound';


configure({ adapter: new Adapter() });
describe('SearchForm', () => {
  it('should render the NotFound component correctly in "debug" mode', () => {
    const component = shallow(<NotFound debug />);
    expect(component).toMatchSnapshot();
  });
});



