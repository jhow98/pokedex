import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import PokemonDetails from '../../components/PokemonDetails';


configure({ adapter: new Adapter() });
describe('SearchForm', () => {
  it('should render the PokemonDetails component correctly in "debug" mode', () => {
    const component = shallow(<PokemonDetails debug />);
    expect(component).toMatchSnapshot();
  });
});



