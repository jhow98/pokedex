import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import SearchForm from '../../components/SearchForm';

configure({ adapter: new Adapter() });
describe('SearchForm', () => {
  it('should render the SearchForm component correctly in "debug" mode', () => {
    const component = shallow(<SearchForm debug />);
    expect(component).toMatchSnapshot();
  });
});
