import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import MainLayout from '../../components/MainLayout';
import PokemonItem from '../../components/PokemonItem';
import Loader from '../../components/Loader';
import SearchForm from '../../components/SearchForm';

const POKEMON_QUERY = gql`
  query LaunchQuery($pokemons_count: Int!) {
    pokemons(first: $pokemons_count) {
      number
      name
      image
      types
    }
  }
`;

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: window.innerHeight,
      pokemons_count: 12,
    };
    this.handleScroll = this.handleScroll.bind(this);
  }
  handleScroll() {
    const windowHeight =
      'innerHeight' in window
        ? window.innerHeight
        : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    );
    const windowBottom = Math.round(windowHeight + window.pageYOffset);
    if (windowBottom >= docHeight) {
      this.setState({
        pokemons_count: this.state.pokemons_count + 12,
      });
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return (
      <MainLayout>
        <div
          className="mr-4"
          style={{
            margin: '0 auto',
            marginTop: '30px',
            width: '80vw',
            minHeight: '80vh',
          }}
        >
          <SearchForm />

          <div className="row">
            <Query
              query={POKEMON_QUERY}
              variables={{ pokemons_count: this.state.pokemons_count }}
            >
              {({ loading, error, data }) => {
                if (loading) return <Loader />;
                if (error) console.log(error);
                let { pokemons } = data;
                return pokemons.map((pokemon) => (
                  <PokemonItem
                    key={parseInt(pokemon.number)}
                    pokemon={pokemon}
                  />
                ));
              }}
            </Query>
          </div>
        </div>
      </MainLayout>
    );
  }
}

export default Home;
