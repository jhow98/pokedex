import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import MainLayout from '../../components/MainLayout';
import PokemonDetails from '../../components/PokemonDetails';
import Loader from '../../components/Loader';
import NotFound from '../../components/NotFound';

const POKEMON_QUERY = gql`
  query LaunchQuery($pokemon_name: String!) {
    pokemon(name: $pokemon_name) {
      id
      number
      name
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      classification
      types
      resistant
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
      weaknesses
      fleeRate
      maxCP
      evolutions {
        id
        number
        name
        image
        types
      }
      evolutionRequirements {
        amount
        name
      }
      maxHP
      image
    }
  }
`;

export class Pokemon extends Component {
  render() {
    let pokemon_name = this.props.match?.params?.pokemon_name;
    return (
      <Query query={POKEMON_QUERY} variables={{ pokemon_name }}>
        {({ loading, error, data }) => {
          if (loading) return <Loader />;
          if (error) console.log(error);
          const { pokemon } = data;
          return pokemon ? (
            <MainLayout>
              <PokemonDetails
                key={pokemon.number.toString()}
                pokemon={pokemon}
              />
            </MainLayout>
          ) : (
            <NotFound
              errorMessage={`Não encontramos o pokemon "${pokemon_name}" na pokedex`}
            />
          );
        }}
      </Query>
    );
  }
}

export default Pokemon;
