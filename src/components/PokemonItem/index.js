import React, { memo } from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import { DetailsLink } from './styles';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const PokemonItem = ({ pokemon }) => {
  return (
    <Card className="col-md-3 mb-4 mr-4">
      <Card.Img
        variant="top"
        src={pokemon?.image}
        fluid="fluid"
        style={{
          objectFit: 'contain',
          height: '200px',
        }}
      />
      <Card.Body>
        <Card.Title>{pokemon?.name}</Card.Title>
      </Card.Body>
      <ListGroup className="list-group-flush">
        {pokemon?.types.map((type, index) => (
          <ListGroupItem key={index.toString()}>{type}</ListGroupItem>
        ))}
      </ListGroup>
      <Card.Body>
        <Link to={`/pokemon/${pokemon?.name}`}>
          <DetailsLink>
            <span className="btn btn-secondary">Detalhes</span>
          </DetailsLink>
        </Link>
      </Card.Body>
    </Card>
  );
};

PokemonItem.propTypes = {
  pokemon: PropTypes.object.isRequired,
};
export default memo(PokemonItem);
