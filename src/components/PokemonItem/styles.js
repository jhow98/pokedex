import styled from 'styled-components';
import * as palette from '../../utils/Variables';

export const DetailsLink = styled.span`
  color: ${palette.THEME_COLOR};
  font-weight: bold;
`;
