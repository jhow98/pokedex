import styled from 'styled-components';
import errorImage from './../../img/404.jpg';
import * as palette from '../../utils/Variables';

export const Container = styled.div`
  background-color: ${palette.THEME_COLOR};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

export const ErrorArea = styled.div`
  h1,
  h3 {
    text-align: center;
    color: ${palette.WHITE_COLOR};
    font-weight: lighter;
  }

  h1 {
    font-weight: normal;
  }
`;

export const Image = styled.div`
  background-color: ${palette.THEME_COLOR};
  background-image: url(${errorImage});
  background-repeat: no-repeat;
  background-position: top;
  background-repeat: no-repeat;
  background-size: contain;
  height: 75vh;
  width: 100%;
`;
