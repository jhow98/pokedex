import React from 'react';
import { Container, ErrorArea, Image } from './styles';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function NotFound({ errorCode, errorMessage }) {
  return (
    <Container>
      <ErrorArea>
        <h3>
          {errorMessage},{' '}
          <Link to="/" className="text-white font-weight-bold">
            volte para o início.
          </Link>
        </h3>
        <h1>{errorCode}</h1>
      </ErrorArea>
      <Image />
    </Container>
  );
}
NotFound.defaultProps = {
  errorCode: 'Erro 404',
  errorMessage: 'Página não encontrada',
};

NotFound.propTypes = {
  errorCode: PropTypes.string,
  errorMessage: PropTypes.string,
};
