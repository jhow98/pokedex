import React from 'react';

const Loader = () => <div className="loader loader-pokeball is-active" />;

export default Loader;
