import React from 'react';
import { Header, Footer, GlobalStyle } from './styles';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function MainLayout({ children }) {
  const containerStyle = {
    margin: '0 auto',
    marginTop: '30px',
    width: '80vw',
    minHeight: '80vh',
  };

  return (
    <>
      <GlobalStyle />
      <main>
        <Link to="/">
          <Header />
        </Link>
        <div className="mr-4" style={containerStyle}>
          <div className="row">{children}</div>
        </div>
        <Footer />
      </main>
    </>
  );
}

MainLayout.propTypes = {
  children: PropTypes.element.isRequired
};
