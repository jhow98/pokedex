import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';
import pokemonLogo from './../../img/pokemon-logo.png';
import pikachu from './../../img/pikachu.png';
import * as palette from '../../utils/Variables';

export const GlobalStyle = createGlobalStyle`
body {
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

html, body, #root {
  height: 100%;
  background-color: ${palette.THEME_COLOR};
}
`;

export const Header = styled.div`
  background-color: ${palette.WHITE_COLOR};
  background-image: url(${pokemonLogo});
  background-repeat: no-repeat;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  padding: 100px;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
  -webkit-box-shadow: 0px 3px 29px -11px rgba(0, 0, 0, 0.49);
  -moz-box-shadow: 0px 3px 29px -11px rgba(0, 0, 0, 0.49);
  box-shadow: 0px 3px 29px -11px rgba(0, 0, 0, 0.49);
`;

export const Footer = styled.div`
  background-color: ${palette.WHITE_COLOR};
  background-image: url(${pikachu});
  background-repeat: no-repeat;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  height: 15vh;
  padding: 100px;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  -webkit-box-shadow: 0px 3px 29px -11px rgba(0, 0, 0, 0.49);
  -moz-box-shadow: 0px 3px 29px -11px rgba(0, 0, 0, 0.49);
  box-shadow: 0px 3px 29px -11px rgba(0, 0, 0, 0.49);
`;
