import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';

export default function Attacks({ attacks }) {
  const [fastAttacks, setFastAttacks] = useState([]);
  const [specialAttacks, setSpecialAttacks] = useState([]);

  const [newFastAttackDamage, setNewFastAttackDamage] = useState(0);
  const [newFastAttackName, setNewFastAttackName] = useState('');
  const [newFastAttackType, setNewFastAttackType] = useState('');

  const [newSpecialAttackDamage, setNewSpecialAttackDamage] = useState(0);
  const [newSpecialAttackName, setNewSpecialAttackName] = useState('');
  const [newSpecialAttackType, setNewSpecialAttackType] = useState('');

  const [toggleFormNewFastAtack, setToggleFormNewFastAtack] = useState(false);
  const [toggleFormNewSpecialAtack, setToggleFormNewSpecialAtack] = useState(
    false
  );

  let fastAttacksList = attacks.fast;
  let specialAttacksList = attacks.special;

  useEffect(() => {
    setFastAttacks(fastAttacksList);
  }, [fastAttacksList]);

  useEffect(() => {
    setSpecialAttacks(specialAttacksList);
  }, [specialAttacksList]);

  const handleDeleteSpecialAttack = ({ attackName }) => {
    setSpecialAttacks(
      specialAttacks.filter((attack) => attack.name !== attackName)
    );
  };

  const handleDeleteFastAttack = ({ attackName }) => {
    setFastAttacks(fastAttacks.filter((attack) => attack.name !== attackName));
  };

  let handleAddFastAttack = (event) => {
    event.preventDefault();

    let newFastAttackItem = {
      damage: newFastAttackDamage,
      name: newFastAttackName,
      type: newFastAttackType,
    };

    setFastAttacks([...fastAttacks, newFastAttackItem]);

    setNewFastAttackDamage('');
    setNewFastAttackName('');
    setNewFastAttackType('');
  };

  let handleAddSpecialAttack = (event) => {
    event.preventDefault();

    let newSpecialAttackItem = {
      damage: newSpecialAttackDamage,
      name: newSpecialAttackName,
      type: newSpecialAttackType,
    };

    setSpecialAttacks([...specialAttacks, newSpecialAttackItem]);

    setNewSpecialAttackDamage('');
    setNewSpecialAttackName('');
    setNewSpecialAttackType('');
  };

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="d-flex flex-row justify-content-between align-items-center">
          <p className="font-weight-bold">Ataques Rápidos</p>{' '}
          <button
            className="btn btn-sm text-white btn-success mr-4 mb-3"
            onClick={() => setToggleFormNewFastAtack(!toggleFormNewFastAtack)}
          >
            {toggleFormNewFastAtack ? 'Fechar' : 'Adicionar'}{' '}
          </button>
        </div>{' '}
        <div className={`row ${!toggleFormNewFastAtack && 'd-none'}`}>
          <div className="col-md-12">
            <form onSubmit={(event) => handleAddFastAttack(event)}>
              <div className="row">
                <div className="col">
                  <label>Dano</label>
                  <input
                    type="number"
                    min="1"
                    className="form-control"
                    placeholder="Dano"
                    name="Damage"
                    value={newFastAttackDamage}
                    onChange={(event) =>
                      setNewFastAttackDamage(event.target.value)
                    }
                    required
                  />
                </div>
                <div className="col">
                  <label>Nome</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nome"
                    name="name"
                    required
                    minLength="3"
                    value={newFastAttackName}
                    onChange={(event) =>
                      setNewFastAttackName(event.target.value)
                    }
                  />
                </div>

                <div className="col-md-12">
                  <label>Tipo</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Tipo"
                    name="type"
                    required
                    minLength="3"
                    value={newFastAttackType}
                    onChange={(event) =>
                      setNewFastAttackType(event.target.value)
                    }
                  />
                </div>
              </div>
              <button type="submit" className="btn btn-primary my-3">
                Salvar
              </button>
            </form>
          </div>
        </div>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Dano</th>
              <th>Nome</th>
              <th>Tipo</th>
              <th>Excluir</th>
            </tr>
          </thead>
          <tbody>
            {fastAttacks?.map((attack, index) => (
              <tr key={index.toString()}>
                <td>{attack.damage}</td>
                <td>{attack.name}</td>
                <td>{attack.type}</td>
                <td
                  onClick={() =>
                    handleDeleteFastAttack({ attackName: attack.name })
                  }
                >
                  <button className="btn btn-sm text-white btn-danger float-right mr-4">
                    X
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>

      <div className="col-md-12 mt-4">
        <div className="d-flex flex-row justify-content-between align-items-center">
          <p className="font-weight-bold">Ataques Especiais</p>{' '}
          <button
            className="btn btn-sm text-white btn-success mr-4 mb-3"
            onClick={() =>
              setToggleFormNewSpecialAtack(!toggleFormNewSpecialAtack)
            }
          >
            {toggleFormNewSpecialAtack ? 'Fechar' : 'Adicionar'}
          </button>
        </div>{' '}
        <div className={`row ${!toggleFormNewSpecialAtack && 'd-none'}`}>
          <div className="col-md-12">
            <form onSubmit={(event) => handleAddSpecialAttack(event)}>
              <div className="row">
                <div className="col">
                  <label>Dano</label>
                  <input
                    type="number"
                    min="1"
                    className="form-control"
                    placeholder="Dano"
                    name="Damage"
                    value={newSpecialAttackDamage}
                    onChange={(event) =>
                      setNewSpecialAttackDamage(event.target.value)
                    }
                    required
                  />
                </div>
                <div className="col">
                  <label>Nome</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nome"
                    name="name"
                    required
                    minLength="3"
                    value={newSpecialAttackName}
                    onChange={(event) =>
                      setNewSpecialAttackName(event.target.value)
                    }
                  />
                </div>

                <div className="col-md-12">
                  <label>Tipo</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Tipo"
                    name="type"
                    required
                    minLength="3"
                    value={newSpecialAttackType}
                    onChange={(event) =>
                      setNewSpecialAttackType(event.target.value)
                    }
                  />
                </div>
              </div>
              <button type="submit" className="btn btn-primary my-3">
                Salvar
              </button>
            </form>
          </div>
        </div>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Dano</th>
              <th>Nome</th>
              <th>Tipo</th>
              <th>Editar</th>
            </tr>
          </thead>
          <tbody>
            {specialAttacks?.map((attack, index) => (
              <tr key={index.toString()}>
                <td>{attack.damage}</td>
                <td>{attack.name}</td>
                <td>{attack.type}</td>
                <td
                  onClick={() =>
                    handleDeleteSpecialAttack({ attackName: attack.name })
                  }
                >
                  <button className="btn btn-sm text-white btn-danger float-right mr-4">
                    X
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
}
