import React from 'react';
import Card from 'react-bootstrap/Card';
import Accordion from 'react-bootstrap/Accordion';
import PropTypes from 'prop-types';
import BasicInfo from './BasicInfo';
import Attacks from './Attacks';
import Evolutions from './Evolutions';


const PokemonDetails = ({ pokemon }) => {
  return (
    <div className="card mb-3 w-100" style={{ marginRight: '20%' }}>
      <div className="row no-gutters">
        <div className="col-md-4">
          <img src={pokemon?.image} className="card-img p-2" alt="Pokemon" />
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title">
              {pokemon?.name}{' '}
              <span className="text-muted text-small">#{pokemon?.number}</span>{' '}
              <span className="float-right d-none d-sm-block">{pokemon?.classification}</span>
            </h5>
            <Accordion defaultActiveKey="0">
              <Card>
                <Card.Header>
                  <Accordion.Toggle variant="link" eventKey="0">
                    Informações básicas
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                   <BasicInfo pokemon={pokemon}/>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Card.Header>
                  <Accordion.Toggle variant="link" eventKey="1">
                    Ataques
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="1">
                  <Card.Body>
                    <Attacks attacks={pokemon?.attacks}/>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Card.Header>
                  <Accordion.Toggle variant="link" eventKey="2">
                    Evoluções
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="2">
                  <Card.Body>
                    <Evolutions pokemon={pokemon}/>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PokemonDetails;

PokemonDetails.propTypes = {
  pokemon: PropTypes.object
};
