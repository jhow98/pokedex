import React from 'react';
import PokemonItem from '../../components/PokemonItem';

export default function Evolutions({ pokemon }) {
  return (
    <>
      <p className="font-weight-bold">Evoluções</p>
      {pokemon.evolutionRequirements?.amount && (
        <p className="mt-3">
          {pokemon.name} necessita de{' '}
          <span className="font-weight-bold">
            {pokemon.evolutionRequirements.amount}{' '}
            {pokemon.evolutionRequirements.name}{' '}
          </span>
          para evoluir.
        </p>
      )}
      <div className="row">
        {pokemon.evolutions ? (
          pokemon.evolutions.map((evolution, index) => (
            <PokemonItem key={index.toString()} pokemon={evolution} />
          ))
        ) : (
          <p className="mt-3 ml-3">Este pokemon não evolui.</p>
        )}
      </div>
    </>
  );
}
