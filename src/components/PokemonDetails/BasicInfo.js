import React from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Badge from 'react-bootstrap/Badge';
import { getTypeColor } from '../../utils';

export default function BasicInfo({pokemon}) {
  return (
    <>
      <div className="row">
        <div className="col-md-6">
          <p className="font-weight-bold">
            Altura mínima:
            <span className="font-weight-normal">
              {' '}
              {pokemon.height.minimum}
            </span>{' '}
          </p>
          <p className="font-weight-bold">
            Altura máxima:
            <span className="font-weight-normal">
              {' '}
              {pokemon.height.maximum}
            </span>{' '}
          </p>
        </div>

        <div className="col-md-6">
          <p className="font-weight-bold">
            Peso mínimo:
            <span className="font-weight-normal">
              {' '}
              {pokemon.weight.minimum}
            </span>{' '}
          </p>
          <p className="font-weight-bold">
            Peso máximo:
            <span className="font-weight-normal">
              {' '}
              {pokemon.weight.maximum}
            </span>{' '}
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <ProgressBar
            variant="success"
            now={pokemon.maxHP / 50}
            label={`HP: ${pokemon.maxHP}`}
          />
          <ProgressBar
            className="mt-4"
            variant="info"
            now={pokemon.maxCP / 50}
            label={`CP: ${pokemon.maxCP}`}
          />
        </div>
      </div>{' '}
      <p className="mt-3">
        {pokemon.name} tem{' '}
        <span className="font-weight-bold">{pokemon.fleeRate} </span>
        de taxa de fulga.
      </p>
      <p className="font-weight-bold">
        Tipos:{' '}
        {pokemon.types.map((type, index) => (
          <Badge
            key={index.toString()}
            className="mr-2"
            variant={getTypeColor(type)}
          >
            {type}
          </Badge>
        ))}
      </p>
      <p className="font-weight-bold">
        Resistente a:{' '}
        {pokemon.resistant.map((value, index) => (
          <Badge
            key={index.toString()}
            className="mr-2"
            variant={getTypeColor(value)}
          >
            {value}
          </Badge>
        ))}
      </p>
      <p className="font-weight-bold">
        Fraquezas:{' '}
        {pokemon.weaknesses.map((value, index) => (
          <Badge
            key={index.toString()}
            className="mr-2"
            variant={getTypeColor(value)}
          >
            {value}
          </Badge>
        ))}
      </p>
    </>
  );
}
