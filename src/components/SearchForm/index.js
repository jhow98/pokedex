import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import { Redirect } from 'react-router-dom';

const SearchForm = () => {
  const [searchText, updateSearchText] = useState('');
  const handleInputChange = (event) => updateSearchText(event.target.value);
  const [redirect, setRedirect] = useState(false);

  const handleFormSubmit = (event) => {
    event.preventDefault();
    if (searchText.trim()) {
      setRedirect(true);
    }
  };

  if (redirect) {
    return <Redirect to={`/pokemon/${searchText}`} />;
  }

  return (
    <div className="row mb-4" style={{ marginRight: '21%' }}>
      <div className="col-md-3" />
      <div className="col-md-6">
        <Form onSubmit={handleFormSubmit}>
          <Form.Group className="d-flex flex-column">
            <Form.Label className="text-align-center d-flex align-self-center text-white h3 pb-2">
              Buscar Pokemón
            </Form.Label>
            <div className="d-flex">
              <Form.Control
                value={searchText}
                onChange={handleInputChange}
                size="lg"
                type="text"
                placeholder="Busca por nome"
              />
              <button onClick={handleFormSubmit} className="btn btn-light ml-2">
                Buscar
              </button>
            </div>
          </Form.Group>
        </Form>
      </div>
      <div className="col-md-3" />
    </div>
  );
};

export default SearchForm;
