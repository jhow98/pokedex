import React from 'react';
import { ApolloProvider } from 'react-apollo';
import apolloClient from './services/apollo';
import Home from './pages/Home';
import Pokemon from './pages/Pokemon';
import NotFound from './components/NotFound';
import 'pure-css-loader';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <Router>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/pokemon/:pokemon_name" component={Pokemon}/>
          <Route component={NotFound}/>
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
