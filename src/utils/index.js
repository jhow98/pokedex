import PropTypes from 'prop-types';

export const getTypeColor = (type) => {
  switch (type) {
    case 'Fire':
      return 'danger';
    case 'Water':
      return 'primary';
    case 'Grass':
      return 'success';
    case 'Flying':
      return 'info';
    case 'Poison':
      return 'warning';
    case 'Fighting':
      return 'dark';
    default:
      return 'secondary';
  }
};

getTypeColor.propTypes = {
  type: PropTypes.string,
};
