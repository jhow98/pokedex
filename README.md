## Sobre o projeto

O projeto Pokedex utiliza, dentre outros, os seguintes recursos:

- [Apollo Client](https://www.apollographql.com/docs/react/)
- [React Router](https://www.npmjs.com/package/react-router)
- [Syled Components](https://styled-components.com/)
- [@testing-library/react](https://testing-library.com/docs/react-testing-library/intro)
- [Prop Types](https://www.npmjs.com/package/prop-types)
- [Bootstrap](https://getbootstrap.com/)

## Instalação em ambiente de desenvolvimento

Executar os seguintes comandos:

1. yarn
2. yarn dev

## Testes

Para executar os testes de páginas e componentes, na raiz do projeto executar o comando abaixo:

yarn test

## License

O React tem sua licença open-source [MIT license](https://opensource.org/licenses/MIT).

## CI/CD

A rotina de CI/CD irá ser executada da seguinte maneira:

1. Commit no branch develop: Build, Testes e Deploy em ambiente de testes (staging). URL: https://pokedex-react-staging.herokuapp.com/
2. Commit no master : Build, Testes e Deploy em ambiente de produção. URL: https://pokedex-react-production.herokuapp.com/
3. Commit nos demais branches: Build e Testes
